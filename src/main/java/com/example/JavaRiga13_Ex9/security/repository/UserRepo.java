package com.example.JavaRiga13_Ex9.security.repository;

import com.example.JavaRiga13_Ex9.security.model.User;
import org.springframework.data.jpa.repository.JpaRepository;

public interface UserRepo extends JpaRepository<User, String> {
}
