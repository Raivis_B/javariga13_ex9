package com.example.JavaRiga13_Ex9.security.repository;

import com.example.JavaRiga13_Ex9.security.model.Role;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.List;

public interface RoleRepo extends JpaRepository<Role, Long> {
    Role findByName(String name);
}
