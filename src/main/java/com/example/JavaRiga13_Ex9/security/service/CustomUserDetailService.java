package com.example.JavaRiga13_Ex9.security.service;

import com.example.JavaRiga13_Ex9.security.repository.UserRepo;
import lombok.RequiredArgsConstructor;
import org.springframework.context.annotation.Profile;
import org.springframework.security.core.authority.SimpleGrantedAuthority;
import org.springframework.security.core.userdetails.User;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.stream.Collectors;

@Profile("demo")
@Service
@RequiredArgsConstructor
public class CustomUserDetailService implements UserDetailsService {

    private final UserRepo userRepo;

    @Override
    public UserDetails loadUserByUsername(String provideUsername) throws UsernameNotFoundException {


        com.example.JavaRiga13_Ex9.security.model.User user = userRepo.findById(provideUsername)
                .orElseThrow(() -> new UsernameNotFoundException("User with name; " + provideUsername + " not found!"));

        List<SimpleGrantedAuthority> authorities =
                user.getRoles().stream()
                .map(r -> new SimpleGrantedAuthority("ROLE_"+r.getName()))
                .collect(Collectors.toList());
        return new User(
                user.getUsername(),
                user.getPassword(),
               authorities
                );
    }
}
