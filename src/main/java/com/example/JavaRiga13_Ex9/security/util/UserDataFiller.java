package com.example.JavaRiga13_Ex9.security.util;

import com.example.JavaRiga13_Ex9.security.model.Role;
import com.example.JavaRiga13_Ex9.security.model.RoleNames;
import com.example.JavaRiga13_Ex9.security.model.User;
import com.example.JavaRiga13_Ex9.security.repository.RoleRepo;
import com.example.JavaRiga13_Ex9.security.repository.UserRepo;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.boot.CommandLineRunner;
import org.springframework.context.annotation.Profile;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Set;
import java.util.stream.Collectors;

@Profile("demo")
@Slf4j
@Service
@RequiredArgsConstructor
public class UserDataFiller implements CommandLineRunner {

    private final UserRepo userRepo;
    private final RoleRepo roleRepo;
    private final BCryptPasswordEncoder encoder;

    @Override
    public void run(String... args) throws Exception {

        log.info("Saving roles!");
        Set<Role> roles = roleRepo.saveAll(
                List.of(
                        Role.builder().name(RoleNames.USER.toString()).build(),
                        Role.builder().name(RoleNames.ADMIN.toString()).build()
                )).stream().collect(Collectors.toSet());

        if (roles == null){
            log.error("Roles not saved!");
            return;
        }

        log.info("Saving users!");
        userRepo.saveAll(
                List.of(
                        User.builder()
                                .username("Raivis")
                                .password(encoder.encode("123"))
                                .roles(roles)
                                .build(),
                        User.builder()
                                .username("RaivisAdmin")
                                .password(encoder.encode("1234"))
                                .roles(roles)
                                .build(),
                        User.builder()
                                .username("Unknown")
                                .password(encoder.encode("123"))
                                .roles(roles)
                                .build()
                )
        );
    }
}
