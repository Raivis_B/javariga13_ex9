package com.example.JavaRiga13_Ex9.security;

import com.example.JavaRiga13_Ex9.security.service.CustomUserDetailService;
import lombok.RequiredArgsConstructor;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Profile;
import org.springframework.core.annotation.Order;
import org.springframework.http.HttpMethod;
import org.springframework.security.authentication.dao.DaoAuthenticationProvider;
import org.springframework.security.config.annotation.authentication.builders.AuthenticationManagerBuilder;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.config.annotation.web.configuration.WebSecurityConfigurerAdapter;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.security.web.util.matcher.AntPathRequestMatcher;

@Profile("demo")
@Configuration
@RequiredArgsConstructor
public class SecurityConf extends WebSecurityConfigurerAdapter {

    private final CustomUserDetailService userDetailService;

    @Bean
    public BCryptPasswordEncoder encoder() {
        return new BCryptPasswordEncoder();
    }



    @Override
    protected void configure(HttpSecurity http) throws Exception {
        http
                .authorizeRequests()
//                .antMatchers("/pc-games").permitAll()
                .antMatchers("/login", "/webjars/**","/register","/h2/**","/api/books/**").permitAll()
                .antMatchers("/pet").hasRole("ADMIN")
                .anyRequest().authenticated()
                .and()
                .formLogin()
                .loginPage("/login")
                .loginProcessingUrl("/login")
                .defaultSuccessUrl("/pc-games", true)
                .and()
                .logout()
                .logoutUrl("/logout")
                .logoutRequestMatcher(new AntPathRequestMatcher("/logout", HttpMethod.GET.toString()))
                .and()
                .headers().frameOptions().disable();

        //šo izslēdz, ja vajag piekļūt h2 vai citām datubāzēm, kad ieslēdz security. Izmantot tikai testa vidē, nekad neizmantot production vidē
        http.csrf().disable();
    }

    @Bean
    public DaoAuthenticationProvider authenticationProvider(BCryptPasswordEncoder encoder, CustomUserDetailService customUserDetailService){
        DaoAuthenticationProvider daoAuthenticationProvider = new DaoAuthenticationProvider();
        daoAuthenticationProvider.setPasswordEncoder(encoder);
        daoAuthenticationProvider.setUserDetailsService(customUserDetailService);
        return daoAuthenticationProvider;
    }

    @Override
    protected void configure(AuthenticationManagerBuilder auth) throws Exception {
//        auth.inMemoryAuthentication()
//                .withUser("Raivis")
//                .password(encoder().encode("123"))
//                .roles("USER")
//                .and()
//                .withUser("RaivisAdmin")
//                .password(encoder().encode("123"))
//                .roles("USER", "ADMIN");

        auth.authenticationProvider(authenticationProvider(encoder(),userDetailService));
    }
}
