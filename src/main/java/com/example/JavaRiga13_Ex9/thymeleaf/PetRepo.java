package com.example.JavaRiga13_Ex9.thymeleaf;

import org.springframework.data.jpa.repository.JpaRepository;

public interface PetRepo extends JpaRepository<Pet, Long> {

}
