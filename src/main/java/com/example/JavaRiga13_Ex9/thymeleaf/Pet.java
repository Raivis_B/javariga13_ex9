package com.example.JavaRiga13_Ex9.thymeleaf;

import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;
import lombok.Data;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;

@Entity
@Data
public class Pet {
    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private Long id;
    @NotBlank(message = "Pet name can't be empty")
    private String name;
    private int age;
    @NotBlank(message = "Pet type can't be empty!")
    private String type;
}
