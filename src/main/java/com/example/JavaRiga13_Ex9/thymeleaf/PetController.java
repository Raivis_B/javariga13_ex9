package com.example.JavaRiga13_Ex9.thymeleaf;

import lombok.RequiredArgsConstructor;
import org.springframework.security.access.annotation.Secured;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.ui.ModelMap;
import org.springframework.validation.BindingResult;
import org.springframework.validation.Errors;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.util.stream.Collectors;

@Controller
@RequiredArgsConstructor
@Secured("ROLE_ADMIN")
@RequestMapping("/pet")

@SessionAttributes("errors")
public class PetController {

    private final PetRepo petRepo;

    @GetMapping
    public String showHomePage(final ModelMap modelMap) {
        modelMap.addAttribute("userName", "Raivis");
        modelMap.addAttribute("newPet", new Pet());
        return "welcome";
    }

    @GetMapping("/all")
    public String showAllPets(final ModelMap modelMap) {
        modelMap.addAttribute("petList", petRepo.findAll());
        return "pet-list";
    }

    @PostMapping("/save")
    public String savePet(final ModelMap modelMap, @Valid Pet pet, Errors errors) {
        if (errors.hasErrors()){
            modelMap.addAttribute(
                    "errors",
                    errors.getAllErrors().stream()
                            .map(error -> error.getDefaultMessage())
                            .collect(Collectors.toList()));

        }else {
            petRepo.save(pet);

        }

        return "redirect:/pet";
    }
}
