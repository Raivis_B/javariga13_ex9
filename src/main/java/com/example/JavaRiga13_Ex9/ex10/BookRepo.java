package com.example.JavaRiga13_Ex9.ex10;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

import java.util.List;
import java.util.Optional;

public interface BookRepo extends JpaRepository<Book, Long> {
    List<Book> findAllByTitle(String title);
//    Optional<Book> findAllByIsbn(String isbn);
//    Optional<Book> findByAuthorAndIsbn(String author, String isbn);
//    List<Book> findTop3ByAuthorOrderByPagesNumDesc(String author);
//    List<Book> findAllTitleStartsWith(String start);
//    List<Book> findAllByPagesNumIsBetween(int min, int max);
//
//    @Query("Select b From books  b where b.pagesNum >= :x")
//    List<Book> findWherePagesNumIsGraterThanX(@Param("x") int x);


}

