package com.example.JavaRiga13_Ex9.ex10;

import lombok.Builder;
import lombok.Data;

import java.util.List;

@Data
@Builder
public class BookWrapper {
    private List<Book> booksList;
}
