package com.example.JavaRiga13_Ex9.ex10;

import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;
import org.springframework.web.bind.annotation.PathVariable;

import java.util.List;
import java.util.Optional;

@Service
@RequiredArgsConstructor
public class BookService {

    private final BookRepo bookRepo;

    public List<Book> getAll() {
        return bookRepo.findAll();
    }


    public List<Book> findAllByTitle(String title) {
        return bookRepo.findAllByTitle(title);
    }

//    public Optional<Book> findAllByIsbn(String isbn){
//        return bookRepo.findAllByIsbn(isbn);
//    }




}
