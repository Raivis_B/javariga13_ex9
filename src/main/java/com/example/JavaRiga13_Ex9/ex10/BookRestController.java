package com.example.JavaRiga13_Ex9.ex10;


import com.example.JavaRiga13_Ex9.FileData;
import lombok.RequiredArgsConstructor;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.*;

import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

@RestController
@RequiredArgsConstructor
@RequestMapping("api/books")

public class BookRestController {

    private final BookService bookService;
    private final BookRepo bookRepo;

    @PostMapping
    @ResponseStatus(HttpStatus.CREATED)
    public Book saveBook(@RequestBody Book book) {
        Book savedBook = bookRepo.save(book);
        return savedBook;
    }

    @PostMapping("/bookslist")
    @ResponseStatus(HttpStatus.CREATED)
    public List<Book> saveAllBooks(@RequestBody List<Book> books) {
        return bookRepo.saveAll(books);
    }

    @GetMapping
    public BookWrapper getAll() {
        return new BookWrapper(bookService.getAll());
    }


    @GetMapping("/{title}")
    public BookWrapper findByTitle(@PathVariable("title") String title) {
        return new BookWrapper(bookService.findAllByTitle(title));
    }

//    @GetMapping("/isbn/{isbn}")
//    public Optional<Book> findAllByIsbn(@PathVariable("isbn") String isbn) {
//        return bookRepo.findAllByIsbn(isbn);
//    }


}
