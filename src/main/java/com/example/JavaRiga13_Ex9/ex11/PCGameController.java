package com.example.JavaRiga13_Ex9.ex11;

import lombok.RequiredArgsConstructor;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;

@Controller
@RequiredArgsConstructor
@RequestMapping("/pc-games")
public class PCGameController {

    private final PCGamesRepo pcGamesRepo;


    @GetMapping
    public String getPcGameFrom(final ModelMap modelMap) {
        modelMap.addAttribute("createMessage", "Create PC Game");
        modelMap.addAttribute("newPcGameForm", new PCGameForm());

        return "pcgames";
    }

    @GetMapping("/pcgame")
    public String showAllGames(final ModelMap modelMap) {
        modelMap.addAttribute("pcgameslist", pcGamesRepo.findAll());
        return "pcgameinfo";
    }

    @PostMapping("/save")
    public String savePCGameForm(final ModelMap modelMap, PCGameForm pcGameForm) {

        modelMap.addAttribute("pcgameslist", pcGamesRepo.save(pcGameForm));


        return "redirect:/pc-games/pcgame";
    }
    


}
