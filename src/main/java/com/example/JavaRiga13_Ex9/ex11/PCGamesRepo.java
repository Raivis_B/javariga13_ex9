package com.example.JavaRiga13_Ex9.ex11;

import org.springframework.data.jpa.repository.JpaRepository;

public interface PCGamesRepo extends JpaRepository<PCGameForm, Long> {
}
