package com.example.JavaRiga13_Ex9;

public class SdaException extends RuntimeException {
    public SdaException(final String message) {
        super(message);
    }
}
