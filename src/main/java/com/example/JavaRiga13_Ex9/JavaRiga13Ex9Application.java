package com.example.JavaRiga13_Ex9;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.autoconfigure.security.servlet.SecurityAutoConfiguration;
import org.springframework.security.config.annotation.method.configuration.EnableGlobalMethodSecurity;


//(exclude = SecurityAutoConfiguration.class)
@SpringBootApplication
@EnableGlobalMethodSecurity(securedEnabled = true)
public class JavaRiga13Ex9Application {

	public static void main(String[] args) {
		SpringApplication.run(JavaRiga13Ex9Application.class, args);
	}

}
